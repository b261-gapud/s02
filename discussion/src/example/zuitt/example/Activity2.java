package example.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Activity2 {

    public static void main(String[] args) {

        //Arrays
        int[] primeNumbers = new int[5];
        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        System.out.println(primeNumbers[0]);

        //ArrayList
        ArrayList<String> friends= new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));

        System.out.println("My friends are:" + friends);

        //Hash map
        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consist of: " + inventory);







    }
}
