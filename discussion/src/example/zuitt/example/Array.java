package example.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    //Java Collection
    //are a single unit of objects
    //useful for manipulating relevant pieces of data that can be used in different situations
    public static void main(String[] args) {
        //Arrays are containers of values of the SAME data type given a pre-defined amount of values
        //Java arrays are more rigid, once the size and data type are defined, they can no longer be change

        //Syntax: Array Declaration
        //dataType[] identifier = new dataType[numOfElements]
        //the values of the array is initializes to 0 or null

        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 98;
        //intArray[5] = 200; //Out of bounds

        System.out.println(intArray[2]);
        //It prints the memory address of the array
        System.out.println(intArray);

        //To print the intArray
        System.out.println(Arrays.toString(intArray));

//        boolean[] bArr = new boolean[2];
//        System.out.println(Arrays.toString(bArr));

        //Declaring Array with Initialization
        //Syntax:
        //dataType[] identifier = {elementA, elementB, ... elementNth};
        String[] names = {"John", "Jane", "Joe"};
        System.out.println(Arrays.toString(names));

        System.out.println(names.length);


        //Sample Java Array methods
        Arrays.sort(intArray);
        System.out.println("Order of items after sort: " + Arrays.toString(intArray));
//        String searchTerm = "John";
//        int binaryResult = Arrays.binarySearch(names,searchTerm);
//        System.out.println(binaryResult);

        //Multi-dimensional Arrays
        //Syntax:
        //dataType[][] identifier = new dataType[rowLength][colLength]

        String[][] classroom = new String[3][3];
        //First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Jane";
        classroom[1][2] = "Jobert";
        //Third Row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        System.out.println(Arrays.toString(classroom));
        //we use the deepToString() method for printing multidimensional array
        System.out.println(Arrays.deepToString(classroom));

        //ArrayLists
        //are resizable arrays, wherein elements can be added or removed whenever it is needed.

        //Syntax:
        //ArrayList<dataType> identifier = new ArrayList<dataType>()

        //Declare an arrayList
//        ArrayList<String> students = new ArrayList<>();
//        //Adding elements to ArrayList is done by the add() function
//        students.add("John");
//        students.add("Paul");
//        System.out.println(students);

        //Declare an ArrayList with values
        ArrayList<String> students = new ArrayList<>(Arrays.asList("John", "Paul"));
        System.out.println(students);

        //Accessing elements to an ArrayList is done by the get() function
        System.out.println(students.get(1));

        //Updating an element
        //arrayListName.set(index, element)
        students.set(1, "George");
        System.out.println(students);

        //Adding an element on a specific index
        //arrayListName.add(index, value/element);
        students.add(1,"Mike");
        System.out.println(students);

        //Removing a specific element
        students.remove(1);
        System.out.println(students);

        //Removing all elements
        students.clear();
        System.out.println(students);

        //Getting the number of elements in an ArrayList
        System.out.println(students.size());

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(1, 2));
        System.out.println(numbers);

        //HashMaps
            //collection of data in "key-value pairs"
            //in Java, "keys" also referred by the "fields"
            //wherein the values are accessed by the "fields"

        //Syntax:
            //HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<dataTypeField, dataTypeValue>();

//        //Declaring HashMaps
//        HashMap<String, String> jobPosition = new HashMap<>();
//        System.out.println(jobPosition);
//
//        //Add elements to HashMap by using the put() function
//        jobPosition.put("Student", "Alice");
//        jobPosition.put("Developer","Magic");
//        System.out.println(jobPosition);

        //Creating HashMap with Initialization
        HashMap<String,String> jobPosition = new HashMap<>() {
            {
                put("Student", "Alice");
                put("Developer", "Magic");
            }
        };
        System.out.println(jobPosition);

        //Accessing elements in HashMaps
        System.out.println(jobPosition.get("Developer"));

        //Updating an element
        jobPosition.replace("Student", "Jacob");
        System.out.println(jobPosition);

        jobPosition.put("Student", "Jacob P");
        System.out.println(jobPosition);

        System.out.println(jobPosition.keySet());
        System.out.println(jobPosition.values());


        jobPosition.remove("Student");
        System.out.println(jobPosition);

        //Removing all elements
        jobPosition.clear();
        System.out.println(jobPosition);


    }

}
